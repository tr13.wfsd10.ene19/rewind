import { Component, OnInit } from '@angular/core';
import { ListaPeliculasService, Peli} from '../../app/servicios/basededatos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {

   peliculas: any [] = [];

 

   constructor(private _moviesService: ListaPeliculasService, private router:Router)
  {
    


   }


  ngOnInit() {
   this._moviesService.getPeliculas().subscribe(datos=>this.peliculas=datos['results']);
  }

}

