import { Injectable } from '@angular/core';
import{ HttpClient} from "@angular/common/http"


@Injectable({
  providedIn: 'root'
})


export class ListaPeliculasService {
peliculas = [];
 urlMoviesdb = 'https://api.themoviedb.org/3';
 apikey = 'b7d0719a3af3f592bb0d30077ad24e5a' 
// En este caso peli es una interfaz, así definimos el tipo. como el privada en principio nadie pyede acceder a peliculas
// peliculas = [];
 // url='http://localhost:4200/assets/peliculas.json';

  constructor(private http:HttpClient) { }
  // Nos creamos un método de la clase que devuelva las películas
  getPeliculas(){
    const movieUrl = `${this.urlMoviesdb}/discover/movie?release_date.gte=1980-01-01&release_date.lte=1990-01-01&api_key=${this.apikey}`;
    return this.http.get<any[]>(movieUrl);
    
  }


 
  getTrailer(movie_id){
    const movieUrl = `${this.urlMoviesdb}/movie/${movie_id}?api_key=${this.apikey}`;
    return this.http.get<any[]>(movieUrl);
    }
 
   
  
  buscarPelicula(nombre){
    const movieUrl = `${this.urlMoviesdb}/search/movie?query=${nombre}&api_key=${this.apikey}`
    return this.http.get<any[]>(movieUrl);
  }
  
}



// Defino la interfaz
export interface Peli{
    id:number;
    nombre:string;
    bio:string;
    img:string;
    aparicion:string;
    trailerId:string;
};
