import { TestBed } from '@angular/core/testing';

import {ListaPeliculasService} from './basededatos.service';

describe('BasededatosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaPeliculasService = TestBed.get(ListaPeliculasService);
    expect(service).toBeTruthy();
  });
});
