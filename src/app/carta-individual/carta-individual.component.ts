import { Component, OnInit, Input } from '@angular/core';
import{Router} from '@angular/router';
import { from } from 'rxjs';
@Component({
  selector: 'app-carta-individual',
  templateUrl: './carta-individual.component.html',
  styleUrls: ['./carta-individual.component.css']
})
export class CartaIndividualComponent implements OnInit {
  @Input() peliSola: any;
  Isvisible: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  idDetallePelicula(id:number){

    this.router.navigate(['/tema',id]);
  }

}
