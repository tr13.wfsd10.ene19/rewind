import { Pipe, PipeTransform } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Pipe({
  name: 'generalpipes'
})
export class GeneralpipesPipe implements PipeTransform {
 
  
  transform(value:string): any {
  value= value.toLocaleLowerCase();
  const palabras: string[]= value.split(' ');
  const capPalabras: string[]=[];
  
  for (let palabra of palabras) {

    palabra= palabra[0].toUpperCase()+ palabra.substr(1);
    capPalabras.push(palabra);
  }
  
  
  
  return capPalabras.join(' ');
  }

}
