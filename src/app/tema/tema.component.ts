import { Component, OnInit } from '@angular/core';
import { ListaPeliculasService, Peli} from '../../app/servicios/basededatos.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-tema',
  templateUrl: './tema.component.html',
  styleUrls: ['./tema.component.css']
})
export class TemaComponent implements OnInit {
  
  peliSola:any;
  
  constructor( private _moviesService: ListaPeliculasService,
    private activateRoute: ActivatedRoute,
    private sanitizer: DomSanitizer
  ) { 
    this.activateRoute.params.subscribe( params => {
      this.peliSola = this._moviesService.getTrailer(params['id']).subscribe(datos=>this.peliSola=datos); 
  });
  }
  ngOnInit() {
  }
  sanitizeUrl(){
    return this.sanitizer.bypassSecurityTrustUrl(this.peliSola.trailerId);
  }

}
