import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { AboutComponent } from './about/about.component';
import { NavComponent } from './nav/nav.component';
import { CartaIndividualComponent } from './carta-individual/carta-individual.component';
import { GeneralpipesPipe } from './generalpipes.pipe';
import { TemaComponent } from './tema/tema.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { EncontradoComponent } from './encontrado/encontrado.component';
import{ HttpClientModule} from "@angular/common/http"

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    PeliculasComponent,
    AboutComponent,
    NavComponent,
    CartaIndividualComponent,
    GeneralpipesPipe,
    TemaComponent,
    BusquedaComponent,
    EncontradoComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
