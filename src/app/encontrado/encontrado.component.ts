import { Component, OnInit } from '@angular/core';
import { ListaPeliculasService, Peli} from '../../app/servicios/basededatos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-encontrado',
  templateUrl: './encontrado.component.html',
  styleUrls: ['./encontrado.component.css']
})
export class EncontradoComponent implements OnInit {
  peliSola: any=[];
  constructor(private _ListaPeliculasService: ListaPeliculasService, private  activatedRoute:ActivatedRoute) { 
    this.activatedRoute.params.subscribe( params => {
      this._ListaPeliculasService.buscarPelicula(params['nombre']).subscribe(datos=>{this.peliSola=datos['results']}); 

   
   });
  }
  

  ngOnInit() { }

}

