import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { AboutComponent } from './about/about.component';
import { TemaComponent } from './tema/tema.component';
import { EncontradoComponent } from './encontrado/encontrado.component';

const routes: Routes = [ 
  {path:'header', component:HeaderComponent},
  {path:'main', component: MainComponent},
  {path:'footer', component: FooterComponent},
  {path:'pelicula' , component: PeliculasComponent},
  {path:'about' , component: AboutComponent},
  {path:'tema/:id',component: TemaComponent},
  {path:'encontrado/:nombre',component: EncontradoComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
